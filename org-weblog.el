;;; org-weblog --- static site generator -*- lexical-binding:t -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; Author: Philippe Estival <pe@7d.nz>
;; Version: 0
;; Package-Requires: ((org-mode "9"))
;; Keywords: static site generator, blog, documentation, org-mode, litterate programming
;; URL: https://gitlab.com/7dnz/org-weblog

;;; Commentary:

;; This package provides a static-site-generator
;; through a single org-mode file

;; To be turn into a mode.

;;; Code:

(save-window-excursion
  (with-current-buffer (find-file "./org-weblog.org")
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-goto-named-src-block "org-blog")
      (org-babel-execute-src-block)
      )))

(provide 'org-weblog)
;;; org-weblog.el ends here
